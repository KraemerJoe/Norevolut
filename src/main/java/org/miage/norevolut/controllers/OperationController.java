package org.miage.norevolut.controllers;


import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.miage.norevolut.assemblers.CardAssembler;
import org.miage.norevolut.assemblers.OperationAssembler;
import org.miage.norevolut.components.IAuthenticationFacade;
import org.miage.norevolut.entities.*;
import org.miage.norevolut.exceptions.NotAllowedOperationException;
import org.miage.norevolut.exceptions.NotEnoughtBalanceException;
import org.miage.norevolut.inputs.CardInput;
import org.miage.norevolut.inputs.OperationInput;
import org.miage.norevolut.repositories.AccountRepository;
import org.miage.norevolut.repositories.CardRepository;
import org.miage.norevolut.repositories.OperationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.persistence.ManyToOne;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.net.URI;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;


@RestController
@RequestMapping(value="/accounts/{accountIban}/operations", produces = MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(Card.class)
public class OperationController {

    private final OperationRepository or;
    private final AccountRepository ar;
    private final OperationAssembler assembler;

    private static final Logger LOGGER = LoggerFactory.getLogger(OperationController.class);

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    public OperationController(OperationRepository or, AccountRepository ar, OperationAssembler assembler){
        this.or = or;
        this.ar = ar;
        this.assembler = assembler;
    }

    // GET all
    @GetMapping
    public ResponseEntity<?> getAllOperationsOfAccount(@PathVariable("accountIban") String iban) {

        String userId = authenticationFacade.getUserId();

        if(authenticationFacade.isAuthorize(ar.getById(iban).getUser().getId(), "manager")){
            return ResponseEntity.ok(assembler.toCollectionModel(or.findAllByDebitedAccountIban(iban)));
        }else{
            LOGGER.info("[FORBIDDEN] User " + userId + " tried to access all operations of " + iban);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    // GET one
    @GetMapping(value="/{operationId}")
    public ResponseEntity<?> getOneOperation(@PathVariable("accountIban") String accountIban, @PathVariable("operationId") String operationId) {

        Optional<Operation> o = or.findById(operationId);

        String userId = authenticationFacade.getUserId();

        return Optional.ofNullable(o)
                .filter(Optional::isPresent)
                .map(i -> {
                    if(authenticationFacade.isAuthorize(o.get().getDebitedAccount().getUser().getId(), "manager")){
                        LOGGER.info("Get operation  " + o.get().getId() + " by user " + userId);
                        return ResponseEntity.ok(assembler.toModel(o.get()));
                    }
                    else {
                        LOGGER.info("[FORBIDDEN] User get operation  " + o.get().getId() + " by user " + userId);
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                    }
                })
                .orElse(ResponseEntity.notFound().build());
    }

    @ExceptionHandler(NotEnoughtBalanceException.class)
    public ResponseEntity<ApiError> handleException(NotEnoughtBalanceException  e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, e), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotAllowedOperationException.class)
    public ResponseEntity<ApiError> handleException(NotAllowedOperationException  e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.FORBIDDEN, e), HttpStatus.FORBIDDEN);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> saveOperation(@PathVariable("accountIban") String accountIban, @RequestBody @Valid OperationInput opInput) throws NotEnoughtBalanceException, NotAllowedOperationException {

        String userId = authenticationFacade.getUserId();

        Optional<Account> account =  ar.findByIban(accountIban);

        if(account.get().getBalance()-opInput.getAmount() < 0){
            LOGGER.info("NOT_ENOUGH_BALANCE by user " + userId + " | Amount: " + opInput.getAmount());
            throw new NotEnoughtBalanceException("NOT_ENOUGH_BALANCE");
        }

        if(!account.get().getUser().getId().equals(userId)){
            throw new NotAllowedOperationException("USER_NOT_ALLOWED_FOR_OPERATION");
        }

        Account acc = account.get();
        account.get().setBalance(acc.getBalance()-opInput.getAmount());
        ar.save(acc);

        Operation operation2Save = new Operation(
                UUID.randomUUID().toString(),
                Instant.now().toString(),
                opInput.getLabel(),
                opInput.getAmount(),
                0.0,
                opInput.getCreditedAccount(),
                account.get(),
                opInput.getCategory(),
                opInput.getCountry(),
                opInput.isTransfert(),
                (opInput.isTransfert() ? "" : opInput.getCardId())
        );

        Operation saved = or.save(operation2Save);
        URI location = linkTo(methodOn(OperationController.class).getOneOperation(saved.getDebitedAccount().getIban(), saved.getId())).toUri();

        LOGGER.info("Operation success by user " + userId + " | Amount: " + opInput.getAmount() + " | Operation: " + saved.getId());

        return ResponseEntity.created(location).build();
    }


}
