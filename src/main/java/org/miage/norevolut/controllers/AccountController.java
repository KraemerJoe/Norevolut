package org.miage.norevolut.controllers;

import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.miage.norevolut.assemblers.AccountAssembler;
import org.miage.norevolut.assemblers.AccountBalanceAssembler;
import org.miage.norevolut.components.IAuthenticationFacade;
import org.miage.norevolut.entities.Account;
import org.miage.norevolut.entities.User;
import org.miage.norevolut.inputs.AccountInput;
import org.miage.norevolut.repositories.AccountRepository;
import org.miage.norevolut.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.Currency;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;



@RestController
@RequestMapping(value="/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(Account.class)
public class AccountController {

    private final AccountRepository ar;
    private final UserRepository ur;
    private final AccountAssembler assembler;
    private final AccountBalanceAssembler assemblerBalance;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    public AccountController(AccountRepository ar, UserRepository ur, AccountAssembler assembler, AccountBalanceAssembler assemblerBalance){
        this.ar = ar;
        this.ur = ur;
        this.assembler = assembler;
        this.assemblerBalance = assemblerBalance;
    }

    @GetMapping
    public ResponseEntity<?> getAllAccounts() {
        String userId = authenticationFacade.getUserId();
        if(authenticationFacade.isAuthorizeByRole("manager"))
            return ResponseEntity.ok(assembler.toCollectionModel(ar.findAll()));
        else
            LOGGER.info("[FORBIDDED] " + userId + "tried to access all accounts data.");
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();

    }

    @GetMapping(value="/{accountIban}")
    public ResponseEntity<?> getOneAccount(@PathVariable("accountIban") String iban) {

        String userId = authenticationFacade.getUserId();

        return Optional.ofNullable(ar.findByIban(iban))
                .filter(Optional::isPresent)
                .map(i -> {
                    EntityModel<Account> acc = assembler.toModel(i.get());

                    boolean authorized = authenticationFacade.isAuthorize(acc.getContent().getUser().getId(), "manager");
                    if(authorized){
                        LOGGER.info("User " + userId + " acceded account " + iban + " infos.");
                        return ResponseEntity.ok(acc);
                    }
                    else{
                        LOGGER.info("[FORBIDDEN] User " + userId + " tried to access account " + iban + " infos.");
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                    }

                })
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(value="/{accountIban}/balance")
    public ResponseEntity<?> getBalanceOfAccount(@PathVariable("accountIban") String iban) {

        String userId = authenticationFacade.getUserId();

        return Optional.ofNullable(ar.findByIban(iban))
                .filter(Optional::isPresent)
                .map(i -> {
                    EntityModel<Account> acc = assembler.toModel(i.get());

                    if(authenticationFacade.isAuthorize(acc.getContent().getUser().getId(), "manager")){
                        LOGGER.info("User " + userId + " acceded account " + iban + " balance.");
                        return ResponseEntity.ok(assemblerBalance.toModel(i.get()));
                    }
                    else{
                        LOGGER.info("[FORBIDDEN] User " + userId + " tried to access account " + iban + " balance.");
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                    }

                })
                .orElse(ResponseEntity.notFound().build());
    }

    @PatchMapping("/{accountIban}")
    public ResponseEntity<?> patchAccount(
            @PathVariable("accountIban") String accountIban, @RequestBody Map<Object, Object> fields) {

        String userId = authenticationFacade.getUserId();

        Account acc = ar.findById(accountIban).get();

        if(authenticationFacade.isAuthorize(acc.getUser().getId(),"manager")){
            LOGGER.info("User " + userId + " patch account " + acc.getIban() + ".");
            fields.forEach((k, v) -> {
                Field field = ReflectionUtils.findField(Account.class, (String) k);
                field.setAccessible(true);
                ReflectionUtils.setField(field, acc, v);
            });

            ar.save(acc);
            return ResponseEntity.ok(assembler.toModel(acc));
        }
        else{
            LOGGER.info("[FORBIDDEN] User " + userId + " tried to patch account " + acc.getIban() + " balance.");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }


    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> saveAccount(@RequestBody @Valid AccountInput account){

        String userId = authenticationFacade.getUserId();

        Iban iban = new Iban.Builder()
                .countryCode(CountryCode.getByCode(account.getCountry()))
                .buildRandom();


        Optional<User> user =  ur.findById(userId);

        Account account2Save = new Account(
                iban.toString(),
                account.getCountry(),
                Currency.getInstance(new Locale("en_GB", account.getCountry())).getCurrencyCode(),
                0.0,
                user.get()
        );

        Account saved = ar.save(account2Save);
        URI location = linkTo(methodOn(AccountController.class).getOneAccount(saved.getIban())).toUri();
        return ResponseEntity.created(location).body(ResponseEntity.ok(assembler.toModel(saved)));
    }
}
