package org.miage.norevolut.controllers;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.miage.norevolut.assemblers.CardAssembler;
import org.miage.norevolut.assemblers.CardViewAssembler;
import org.miage.norevolut.components.IAuthenticationFacade;
import org.miage.norevolut.entities.Account;
import org.miage.norevolut.entities.Card;
import org.miage.norevolut.inputs.CardInput;
import org.miage.norevolut.repositories.AccountRepository;
import org.miage.norevolut.repositories.CardRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.net.URI;
import java.time.Instant;
import java.time.Month;
import java.time.Year;
import java.time.ZoneId;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;


@RestController
@RequestMapping(value="/accounts/{accountIban}/cards", produces = MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(Card.class)
public class CardController {

    private final CardRepository cr;
    private final AccountRepository ar;
    private final CardAssembler assembler;
    private final CardViewAssembler cardViewAssembler;

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    private static final Logger LOGGER = LoggerFactory.getLogger(CardController.class);

    public CardController(CardRepository cr, AccountRepository ar, CardAssembler assembler, CardViewAssembler cardViewAssembler){
        this.cr = cr;
        this.ar = ar;
        this.assembler = assembler;
        this.cardViewAssembler = cardViewAssembler;
    }

    // GET all
    @GetMapping
    public ResponseEntity<?> getAllCardsOfAccount(@PathVariable("accountIban") String iban) {

        String userId = authenticationFacade.getUserId();

        if(authenticationFacade.isAuthorize(ar.getById(iban).getUser().getId(), "manager")){
            return ResponseEntity.ok(cardViewAssembler.toCollectionModel(cr.findAllByAccountIban(iban)));
        }else{
            LOGGER.info("[FORBIDDEN] User " + userId + " tried to access all cards of " + iban);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }


    }

    // GET one
    @GetMapping(value="/{cardId}")
    public ResponseEntity<?> getOneCard(@PathVariable("accountIban") String accountIban, @PathVariable("cardId") String cardId) {

        Optional<Card> u = cr.findById(cardId);

        String userId = authenticationFacade.getUserId();

        return Optional.ofNullable(u)
                .filter(Optional::isPresent)
                .map(i -> {
                    if(authenticationFacade.isAuthorize(u.get().getAccount().getUser().getId(), "manager")){
                        LOGGER.info("User " + userId + " get info of card " + u.get().getNumber());
                        return ResponseEntity.ok(cardViewAssembler.toModel(u.get()));
                    }
                    else {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                    }
                })
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> saveCard(@PathVariable("accountIban") String accountIban, @RequestBody @Valid CardInput cardInput){

        String userId = authenticationFacade.getUserId();

        Optional<Account> acc =  ar.findByIban(accountIban);

        if(!acc.isPresent()){
            return ResponseEntity.badRequest().build();
        }

        Account account = acc.get();

        if(!userId.equalsIgnoreCase(account.getUser().getId())){
            LOGGER.info("[FORBIDDEN] User " + userId + " tried to create a card.");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        Instant instant = Instant.now();
        Month m = Month.from(instant.atZone(ZoneId.of("Europe/Paris")));
        Year y = Year.from(instant.atZone(ZoneId.of("Europe/Paris")));
        Card card2Save = new Card(
                UUID.randomUUID().toString(),
                getCardNumber(16),
                getCardNumber(4),
                getCardNumber(3),
                String.valueOf(m.getValue()),
                String.valueOf(y.getValue()+3),
                false,
                false,
                1000.0D,
                false,
                cardInput.isVirtual(),
                account,
                instant.toString()
        );

        Card saved = cr.save(card2Save);
        URI location = linkTo(methodOn(CardController.class).getOneCard(saved.getAccount().getIban(), saved.getId())).toUri();

        LOGGER.info("User " + userId + " created a new card with number " + saved.getNumber());

        return ResponseEntity.created(location).body(ResponseEntity.ok(cardViewAssembler.toModel(saved)));
    }

    @PatchMapping("/{cardId}")
    public ResponseEntity<?> patch(
            @PathVariable("accountIban") String accountIban, @PathVariable("cardId") String cardId, @RequestBody Map<Object, Object> fields) {

        String userId = authenticationFacade.getUserId();

        Card card = cr.findById(cardId).get();

        if(authenticationFacade.isAuthorize(card.getAccount().getUser().getId(), "manager")){
            fields.forEach((k, v) -> {
                Field field = ReflectionUtils.findField(Card.class, (String) k);
                field.setAccessible(true);
                ReflectionUtils.setField(field, card, v);
            });

            cr.save(card);
            LOGGER.info("User " + userId + " modified the card " + card.getNumber());
            return ResponseEntity.ok(cardViewAssembler.toModel(card));
        }else{
            LOGGER.info("[FORBIDDEN] User " + userId + " tried to modify the card " + card.getNumber());
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }


    }

    private String getCardNumber(int l) {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < l) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

}
