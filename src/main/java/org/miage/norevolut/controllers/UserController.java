package org.miage.norevolut.controllers;


import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.miage.norevolut.NorevolutApplication;
import org.miage.norevolut.assemblers.UserAssembler;
import org.miage.norevolut.components.IAuthenticationFacade;
import org.miage.norevolut.entities.User;
import org.miage.norevolut.inputs.UserInput;
import org.miage.norevolut.repositories.UserRepository;
import org.miage.norevolut.validators.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.lang.reflect.Field;
import java.net.URI;
import java.security.Principal;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping(value="/users", produces = MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(User.class)
public class UserController {

    private final UserRepository ur;
    private final UserAssembler assembler;
    private final UserValidator validator;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private IAuthenticationFacade authenticationFacade;

    public UserController(UserRepository ur, UserAssembler assembler, UserValidator validator){
        this.ur = ur;
        this.assembler = assembler;
        this.validator = validator;
    }

    @GetMapping
    public ResponseEntity<?> getAllUsers() {
        String userId = authenticationFacade.getUserId();
        if(authenticationFacade.isAuthorizeByRole("manager"))
            return ResponseEntity.ok(assembler.toCollectionModel(ur.findAll()));
        else
            LOGGER.info("[FORBIDDED] " + userId + "tried to access all users data.");
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    @GetMapping(value="/{userId}")
    public ResponseEntity<?> getOneUser(@PathVariable("userId") String id) {

        Optional<User> u = ur.findById(id);
        String userId = authenticationFacade.getUserId();

        return Optional.ofNullable(u)
                .filter(Optional::isPresent)
                .map(i -> {
                    EntityModel<User> user = assembler.toModel(i.get());
                    boolean authorized = authenticationFacade.isAuthorize(user.getContent().getId(), "manager");
                    if(authorized){
                        LOGGER.info(userId + " acceded user " + user.getContent().getId() + " data.");
                        return ResponseEntity.ok(user);
                    }
                    else{
                        LOGGER.info("[FORBIDDED] " + userId + "tried to access user " + user.getContent().getId() + " data.");
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                    }
                })
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> saveUser(@RequestBody @Valid UserInput user){

        KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccessToken accessToken = keycloakPrincipal.getKeycloakSecurityContext().getToken();
        String idKeycloak = "";

        if (keycloakPrincipal instanceof UserDetails) {
            idKeycloak = ((UserDetails)keycloakPrincipal).getUsername();
        } else {
            idKeycloak = keycloakPrincipal.toString();
        }

        User user2Save = new User(
                idKeycloak,
                accessToken.getGivenName(),
                accessToken.getFamilyName(),
                accessToken.getEmail(),
                accessToken.getBirthdate(),
                user.getPhone(),
                "default"
        );

        User saved = ur.save(user2Save);
        URI location = linkTo(UserController.class).slash(saved.getId()).toUri();
        LOGGER.info(idKeycloak + " created user " + user2Save.getId() + " data.");

        return ResponseEntity.created(location).body(ResponseEntity.ok(assembler.toModel(saved)));
    }


}
