package org.miage.norevolut.controllers;



import org.miage.norevolut.entities.Account;
import org.miage.norevolut.entities.ApiError;
import org.miage.norevolut.entities.Card;
import org.miage.norevolut.entities.Operation;
import org.miage.norevolut.exceptions.CardInfoException;
import org.miage.norevolut.exceptions.CardLockedException;
import org.miage.norevolut.exceptions.NotEnoughtBalanceException;
import org.miage.norevolut.exceptions.UnknowCardException;
import org.miage.norevolut.inputs.PaymentInput;
import org.miage.norevolut.repositories.AccountRepository;
import org.miage.norevolut.repositories.CardRepository;
import org.miage.norevolut.repositories.OperationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;


@RestController
@RequestMapping(value="/payments", produces = MediaType.APPLICATION_JSON_VALUE)
@ExposesResourceFor(Card.class)
public class PaymentController {

    private final OperationRepository or;
    private final AccountRepository ar;
    private final CardRepository cr;

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);

    public PaymentController(OperationRepository or, AccountRepository ar, CardRepository cr){
        this.or = or;
        this.ar = ar;
        this.cr = cr;
    }

    @ExceptionHandler(CardInfoException.class)
    public ResponseEntity<ApiError> handleException(CardInfoException  e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, e), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnknowCardException.class)
    public ResponseEntity<ApiError> handleException(UnknowCardException  e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, e), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotEnoughtBalanceException.class)
    public ResponseEntity<ApiError> handleException(NotEnoughtBalanceException  e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, e), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CardLockedException.class)
    public ResponseEntity<ApiError> handleException(CardLockedException  e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, e), HttpStatus.BAD_REQUEST);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> saveOperation(@RequestBody @Valid PaymentInput payInput) throws CardInfoException, UnknowCardException, NotEnoughtBalanceException, CardLockedException {

        Optional<Card> card = cr.findByNumber(payInput.getCardNumber());

        if(!card.isPresent()){
            throw new UnknowCardException("UNKNOW_CARD");
        }

        Card cardGet = card.get();

        if(cardGet.isLocked()){
            LOGGER.info("CARD_LOCKED_EXCEPTION for the card " + cardGet.getNumber());
            throw new CardLockedException("CARD_LOCKED_EXCEPTION");
        }

        if(
                !(cardGet.getNumber().equalsIgnoreCase(payInput.getCardNumber()))
                || !(cardGet.getCryptogram().equalsIgnoreCase(payInput.getCardCryptogram()))
            || !(cardGet.getExpiryMonth().equalsIgnoreCase(payInput.getExpiryMonth()))
            || !(cardGet.getExpiryYear().equalsIgnoreCase(payInput.getExpiryYear()))
        ){
            LOGGER.info("INVALID_CARD_INFO for the card " + cardGet.getNumber());
            throw new CardInfoException("INVALID_CARD_INFO");
        }

        if(card.get().getAccount().getBalance()-payInput.getAmount() < 0){
            LOGGER.info("NOT_ENOUGH_BALANCE for the card " + cardGet.getNumber());
            throw new NotEnoughtBalanceException("NOT_ENOUGH_BALANCE");
        }


        Card cGet = card.get();
        Account acc = cGet.getAccount();


        acc.setBalance(acc.getBalance()-payInput.getAmount());


        Operation operation2Save = new Operation(
                UUID.randomUUID().toString(),
                Instant.now().toString(),
                payInput.getLabel(),
                payInput.getAmount(),
                0.0,
                payInput.getMerchant(),
               acc,
                payInput.getCategory(),
                payInput.getCountry(),
                false,
                payInput.getCardNumber()
        );

        if(cGet.isVirtual()) {
            cr.delete(cGet);
            LOGGER.info("Virtual card used and deleted " + cardGet.getNumber());
        }

        ar.save(acc);

        Operation saved = or.save(operation2Save);

        LOGGER.info("PAYMENT SUCCESS for the card " + cardGet.getNumber() + " | Amount: " + payInput.getAmount());
        return ResponseEntity.ok().build();
    }


}
