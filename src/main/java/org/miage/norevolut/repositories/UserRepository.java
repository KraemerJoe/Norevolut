package org.miage.norevolut.repositories;

import org.miage.norevolut.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, String> {

}
