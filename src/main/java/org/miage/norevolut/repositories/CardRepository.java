package org.miage.norevolut.repositories;

import org.miage.norevolut.entities.Card;
import org.miage.norevolut.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CardRepository extends JpaRepository<Card, String> {

    Iterable<? extends Card> findAllByAccountIban(String iban);

    Optional<Card> findByNumber(String cardNumber);
}
