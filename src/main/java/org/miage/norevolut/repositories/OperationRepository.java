package org.miage.norevolut.repositories;

import org.miage.norevolut.entities.Card;
import org.miage.norevolut.entities.Operation;
import org.miage.norevolut.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperationRepository extends JpaRepository<Operation, String> {
    Iterable<? extends Operation> findAllByDebitedAccountIban(String iban);
}
