package org.miage.norevolut.repositories;


import org.miage.norevolut.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, String> {

    Optional<Account> findByIban(String iban);
}
