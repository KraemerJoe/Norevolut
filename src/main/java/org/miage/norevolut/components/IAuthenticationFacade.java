package org.miage.norevolut.components;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.miage.norevolut.NorevolutApplication;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Null;

public interface IAuthenticationFacade {
    Authentication getAuthentication();
    boolean isAuthorize(String needToPass, String bypassRole);
    boolean isAuthorizeByRole(String bypassRole);
    String getUserId();

}
@Component
class AuthenticationFacade implements IAuthenticationFacade {

    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public boolean isAuthorize(String needToPass, String bypassRole){
        if(NorevolutApplication.testing){
            return true;
        }
        KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccessToken accessToken = keycloakPrincipal.getKeycloakSecurityContext().getToken();

        if(accessToken.getRealmAccess().getRoles().contains(bypassRole)) return true;
        return keycloakPrincipal.toString().equalsIgnoreCase(needToPass);
    }

    @Override
    public boolean isAuthorizeByRole(String bypassRole){
        if(NorevolutApplication.testing){
            return true;
        }
        KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccessToken accessToken = keycloakPrincipal.getKeycloakSecurityContext().getToken();

        if(accessToken.getRealmAccess().getRoles().contains(bypassRole)) return true;
        return false;
    }

    @Override
    public String getUserId(){
        if(NorevolutApplication.testing){
            if(!NorevolutApplication.testingUser.equalsIgnoreCase("")){
                return NorevolutApplication.testingUser;
            }
            return "testingUser";
        }
        KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return keycloakPrincipal.toString();
    }
}