package org.miage.norevolut.exceptions;

public class NotEnoughtBalanceException extends Exception {
    public NotEnoughtBalanceException(String errorMessage) {
        super(errorMessage);
    }
}