package org.miage.norevolut.exceptions;

public class CardInfoException extends Exception {
    public CardInfoException(String errorMessage) {
        super(errorMessage);
    }
}