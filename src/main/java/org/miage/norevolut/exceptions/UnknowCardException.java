package org.miage.norevolut.exceptions;

public class UnknowCardException extends Exception {
    public UnknowCardException(String errorMessage) {
        super(errorMessage);
    }
}