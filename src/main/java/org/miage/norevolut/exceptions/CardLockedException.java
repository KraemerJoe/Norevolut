package org.miage.norevolut.exceptions;

public class CardLockedException extends Exception {
    public CardLockedException(String errorMessage) {
        super(errorMessage);
    }
}