package org.miage.norevolut.exceptions;

public class NotAllowedOperationException extends Exception {
    public NotAllowedOperationException(String errorMessage) {
        super(errorMessage);
    }
}