package org.miage.norevolut.assemblers;

import org.miage.norevolut.controllers.CardController;
import org.miage.norevolut.controllers.UserController;
import org.miage.norevolut.entities.Card;
import org.miage.norevolut.entities.User;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CardAssembler implements RepresentationModelAssembler<Card, EntityModel<Card>> {

  @Override
  public EntityModel<Card> toModel(Card card) {
    return EntityModel.of(card,
            linkTo(methodOn(CardController.class).getOneCard(card.getAccount().getIban(), card.getId())).withSelfRel(),
            linkTo(methodOn(CardController.class).getAllCardsOfAccount(card.getAccount().getIban())).withRel("collection"));
  }


}