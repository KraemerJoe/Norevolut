package org.miage.norevolut.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.miage.norevolut.components.IAuthenticationFacade;
import org.miage.norevolut.controllers.UserController;
import org.miage.norevolut.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class UserAssembler implements RepresentationModelAssembler<User, EntityModel<User>> {



  @Override
  public EntityModel<User> toModel(User user) {
    return EntityModel.of(user,
            linkTo(methodOn(UserController.class).getOneUser(user.getId())).withSelfRel(),
            linkTo(methodOn(UserController.class).getAllUsers()).withRel("collection"));
  }

  @Override
  public CollectionModel<EntityModel<User>> toCollectionModel(Iterable<? extends User> entities) {
      List<EntityModel<User>> intervenantModel = StreamSupport
        				.stream(entities.spliterator(), false)
        				.map(i -> toModel(i))
        				.collect(Collectors.toList());
      return CollectionModel.of(intervenantModel,                                					
              linkTo(methodOn(UserController.class)
               			.getAllUsers()).withSelfRel());
  }
}