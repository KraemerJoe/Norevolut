package org.miage.norevolut.assemblers;


import org.miage.norevolut.controllers.AccountController;
import org.miage.norevolut.entities.Account;
import org.miage.norevolut.entities.views.AccountBalance;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public class AccountBalanceAssembler implements RepresentationModelAssembler<Account, AccountBalance> {

    @Override
    public AccountBalance toModel(Account account) {
        AccountBalance accountBalance = new AccountBalance(account);
        accountBalance.add(linkTo(methodOn(AccountController.class).getBalanceOfAccount(account.getIban())).withSelfRel());
        accountBalance.add(linkTo(methodOn(AccountController.class).getOneAccount(account.getIban())).withRel("account"));
        return accountBalance;
    }


}
