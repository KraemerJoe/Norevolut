package org.miage.norevolut.assemblers;

import org.miage.norevolut.controllers.OperationController;
import org.miage.norevolut.entities.Operation;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class OperationAssembler implements RepresentationModelAssembler<Operation, EntityModel<Operation>> {

  @Override
  public EntityModel<Operation> toModel(Operation o) {
    return EntityModel.of(o,
            linkTo(methodOn(OperationController.class).getOneOperation(o.getDebitedAccount().getIban(), o.getId())).withSelfRel(),
            linkTo(methodOn(OperationController.class).getAllOperationsOfAccount(o.getDebitedAccount().getIban())).withRel("collection"));
  }


}