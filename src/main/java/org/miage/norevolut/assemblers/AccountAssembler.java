package org.miage.norevolut.assemblers;


import org.miage.norevolut.controllers.AccountController;
import org.miage.norevolut.controllers.OperationController;
import org.miage.norevolut.controllers.UserController;
import org.miage.norevolut.entities.Account;
import org.springframework.hateoas.EntityModel;

import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public class AccountAssembler implements RepresentationModelAssembler<Account, EntityModel<Account>> {

    @Override
    public EntityModel<Account> toModel(Account account) {

        return EntityModel.of(account,
                linkTo(methodOn(AccountController.class).getOneAccount(account.getIban())).withSelfRel(),
                linkTo(methodOn(AccountController.class).getAllAccounts()).withRel("collection"),
                linkTo(methodOn(OperationController.class).getAllOperationsOfAccount(account.getIban())).withRel("operations"),
                linkTo(methodOn(UserController.class).getOneUser(account.getUser().getId())).withRel("user"));
    }



}
