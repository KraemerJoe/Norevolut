package org.miage.norevolut.assemblers;

import org.miage.norevolut.controllers.AccountController;
import org.miage.norevolut.controllers.CardController;
import org.miage.norevolut.entities.Card;
import org.miage.norevolut.entities.views.CardView;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.core.DummyInvocationUtils;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public class CardViewAssembler implements RepresentationModelAssembler<Card, CardView> {

  @Override
  public CardView toModel(Card card) {
    CardView accountBalance = new CardView(card);
    accountBalance.add(linkTo(DummyInvocationUtils.methodOn(CardController.class).getOneCard(card.getAccount().getIban(), card.getId())).withSelfRel());
    accountBalance.add(linkTo(DummyInvocationUtils.methodOn(CardController.class).getAllCardsOfAccount(card.getAccount().getIban())).withRel("cards"));
    accountBalance.add(linkTo(DummyInvocationUtils.methodOn(AccountController.class).getOneAccount(card.getAccount().getIban())).withRel("account"));
    return accountBalance;
  }


}