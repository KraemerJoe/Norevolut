package org.miage.norevolut.validators;

import org.miage.norevolut.inputs.CardInput;
import org.miage.norevolut.inputs.UserInput;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

@Service
public class CardValidator {

  private Validator validator;

  CardValidator(Validator validator) {
    this.validator = validator;
  }

  public void validate(CardInput user) {
    Set<ConstraintViolation<CardInput>> violations = validator.validate(user);
    if (!violations.isEmpty()) {
      throw new ConstraintViolationException(violations);
    }
  }
}
