package org.miage.norevolut.validators;

import org.miage.norevolut.inputs.OperationInput;
import org.miage.norevolut.inputs.UserInput;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

@Service
public class OperationValidator {

  private Validator validator;

  OperationValidator(Validator validator) {
    this.validator = validator;
  }

  public void validate(OperationInput o) {
    Set<ConstraintViolation<OperationInput>> violations = validator.validate(o);
    if (!violations.isEmpty()) {
      throw new ConstraintViolationException(violations);
    }
  }
}
