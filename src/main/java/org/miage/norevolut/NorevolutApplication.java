package org.miage.norevolut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class NorevolutApplication {

	public static boolean testing = false;
    public static String testingUser = "";

    public static void main(String[] args) {
		SpringApplication.run(NorevolutApplication.class, args);
	}

}
