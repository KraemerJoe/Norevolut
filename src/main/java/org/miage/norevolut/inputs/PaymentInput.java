package org.miage.norevolut.inputs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentInput {

   private String cardNumber;
   private String cardCryptogram;
   private String expiryMonth;
   private String expiryYear;
   private double amount;
   private String merchant;
   private String label;
   private String category;
   private String country;

}
