package org.miage.norevolut.inputs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.miage.norevolut.entities.Account;
import org.springframework.lang.Nullable;

import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OperationInput {

    private String label;
    private Double amount;

    private String creditedAccount;

    @Nullable
    private String category;
    private String country;

    private boolean transfert;

    @Nullable
    private String cardId;


}
