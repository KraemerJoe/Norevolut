package org.miage.norevolut.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {

    @Id
    @Column(updatable = false)
    private String iban;

    private String country;

    private String currency;

    @Column(updatable = false)
    private Double balance;

    @ManyToOne
    private User user;

}
