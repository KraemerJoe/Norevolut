package org.miage.norevolut.entities;

import lombok.*;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.Instant;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {


    @Id
    @Column(updatable = false)
    private String id;

    private String firstname;
    private String lastname;

    @Column(unique=true)
    private String email;

    private String birthDate;
    private String phone;

    @JsonIgnore
    private String secret;
}
