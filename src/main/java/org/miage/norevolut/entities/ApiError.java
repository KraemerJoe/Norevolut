package org.miage.norevolut.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;


@Setter
@Getter
public class ApiError {

   private HttpStatus status;
   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
   private LocalDateTime timestamp;
   private String message;


   public ApiError() {
       timestamp = LocalDateTime.now();
   }

    public ApiError(HttpStatus status) {
       this();
       this.status = status;
   }

    public ApiError(HttpStatus status, Throwable ex) {
       this();
       this.status = status;
       this.message = ex.getMessage();
   }

   public ApiError(HttpStatus status, String message, Throwable ex) {
       this();
       this.status = status;
       this.message = message;
   }
}