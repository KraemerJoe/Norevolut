package org.miage.norevolut.entities;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Operation implements Serializable {

    @Id
    @Column(updatable = false)
    private String id;

    @Column(updatable = false)
    private String date;

    @Column(updatable = false)
    private String label;
    @Column(updatable = false)
    private Double amount;

    @Column(updatable = false)
    private Double rate;

    @Column(updatable = false)
    private String creditedAccount;

    @ManyToOne
    private Account debitedAccount;

    @Nullable
    private String category;

    @Column(updatable = false)
    @Pattern(regexp = "[a-zA-Z]{2,}")
    private String country;

    @Column(updatable = false)
    private boolean isTransfert;

    private String card;

}
