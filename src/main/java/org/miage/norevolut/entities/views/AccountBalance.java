package org.miage.norevolut.entities.views;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.miage.norevolut.entities.Account;
import org.springframework.hateoas.RepresentationModel;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class AccountBalance extends RepresentationModel<AccountBalance> {


    private double balance;

    public AccountBalance(Account acc) {
        this.balance = acc.getBalance();
    }
}
