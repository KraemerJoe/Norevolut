package org.miage.norevolut.entities.views;

import lombok.*;
import org.miage.norevolut.entities.Account;
import org.miage.norevolut.entities.Card;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;
import java.time.Instant;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CardView extends RepresentationModel<CardView>  {

    @Id
    @Column(updatable = false)
    private String id;

    @Column(updatable = false)
    @Pattern(regexp = "^[0-9]{16}$")
    private String number;

    private String code;
    private String crypto;
    private String expiryMonth;
    private String expiryYear;

    private boolean locked;
    private boolean localisation;
    private double ceiling;
    private boolean contactless;

    @Column(updatable = false)
    private boolean virtual;

    private String account;

    private String creationDate;

    public CardView(Card card) {
        this.id = card.getId();
        this.number = card.getNumber();
        this.crypto = card.getCryptogram();
        this.expiryMonth = card.getExpiryMonth();
        this.expiryYear = card.getExpiryYear();
        this.code = card.getCode();
        this.locked = card.isLocked();
        this.localisation = card.isLocalisation();
        this.ceiling = card.getCeiling();
        this.contactless = card.isContactless();
        this.virtual = card.isVirtual();
        this.account = card.getAccount().getIban();
        this.creationDate = card.getCreationDate();
    }
}