package org.miage.norevolut.entities;

import lombok.*;
import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.time.Instant;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Card {

    @Id
    @Column(updatable = false)
    private String id;

    @Column(updatable = false)
    @Pattern(regexp = "^[0-9]{16}$")
    private String number;

    @Pattern(regexp = "^[0-9]{4}$")
    private String code;

    @Pattern(regexp = "^[0-9]{3}$")
    private String cryptogram;

    private String expiryMonth;
    private String expiryYear;

    private boolean locked;
    private boolean localisation;
    private double ceiling;
    private boolean contactless;

    @Column(updatable = false)
    private boolean virtual;

    @ManyToOne
    private Account account;

    private String creationDate;
}