package org.miage.norevolut.auth;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;


public class CustomMethodSecurityExpression extends SecurityExpressionRoot
        implements MethodSecurityExpressionOperations {

    public CustomMethodSecurityExpression(Authentication authentication) {
        super(authentication);
    }


    public boolean hasSpecifiedRole(String role) {
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) this.authentication;
        KeycloakPrincipal principal = (KeycloakPrincipal) token.getPrincipal();
        AccessToken accessToken = principal.getKeycloakSecurityContext().getToken();
        if(accessToken.getRealmAccess().getRoles().contains(role)){
            return true;
        }
        return false;
    }

    @Override
    public void setFilterObject(Object o) {
    }

    @Override
    public Object getFilterObject() {
        return null;
    }

    @Override
    public void setReturnObject(Object returnObject) {

    }

    @Override
    public Object getReturnObject() {
        return null;
    }

    @Override
    public Object getThis() {
        return null;
    }
}