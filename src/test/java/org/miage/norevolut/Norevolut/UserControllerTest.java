package org.miage.norevolut.Norevolut;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.miage.norevolut.NorevolutApplication;
import org.miage.norevolut.entities.User;
import org.miage.norevolut.repositories.AccountRepository;
import org.miage.norevolut.repositories.CardRepository;
import org.miage.norevolut.repositories.OperationRepository;
import org.miage.norevolut.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static io.restassured.RestAssured.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {

	@LocalServerPort
	int port;

	@Autowired
	UserRepository ur;

	@Autowired
	OperationRepository or;

	@Autowired
	CardRepository cr;

	@Autowired
	AccountRepository ar;


	@BeforeEach
	public void setupContext(){
		or.deleteAll();
		cr.deleteAll();
		ar.deleteAll();
		ur.deleteAll();
		RestAssured.port = port;
		NorevolutApplication.testing = true;
	}

	@Test
	public void getOneUser(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);
		Response response = when().get("/norevolut/users/" + i1.getId())
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract()
				.response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString("Tom"));
	}

	@Test
	public void getAllUsers(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);
		User i2 = new User(UUID.randomUUID().toString(), "Hugo", "Martin", "test2@gmail.com", "54000", "003364878666", "");
		ur.save(i2);
		User i3 = new User(UUID.randomUUID().toString(), "Lolo", "Dupont", "test3@gmail.com", "54000", "003364878666", "");
		ur.save(i3);
		Response response = when().get("/norevolut/users")
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract()
				.response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString("Tom"));
		assertThat(jsonAsString, containsString("Hugo"));
		assertThat(jsonAsString, containsString("Dupont"));
	}


}
