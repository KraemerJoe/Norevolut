package org.miage.norevolut.Norevolut;

import com.nimbusds.jose.shaded.json.JSONObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.miage.norevolut.NorevolutApplication;
import org.miage.norevolut.entities.Account;
import org.miage.norevolut.entities.Card;
import org.miage.norevolut.entities.Operation;
import org.miage.norevolut.entities.User;
import org.miage.norevolut.repositories.AccountRepository;
import org.miage.norevolut.repositories.CardRepository;
import org.miage.norevolut.repositories.OperationRepository;
import org.miage.norevolut.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.time.Instant;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PaymentControllerTest {

	@LocalServerPort
	int port;

	@Autowired
	UserRepository ur;

	@Autowired
	OperationRepository or;

	@Autowired
	CardRepository cr;

	@Autowired
	AccountRepository ar;


	@BeforeEach
	public void setupContext(){
		or.deleteAll();
		cr.deleteAll();
		ar.deleteAll();
		ur.deleteAll();
		RestAssured.port = port;
		NorevolutApplication.testing = true;
	}

	@Test
	public void unknowCard(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("cardNumber", "1234567891234567");
		requestParams.put("expiryMonth", "01");
		requestParams.put("expiryYear", "2025");
		requestParams.put("cardCryptogram", "123");
		requestParams.put("amount", 100);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/payments")
				.then()
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.extract().response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString("UNKNOW_CARD"));
	}

	@Test
	public void cardLocked(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		Card c1 = new Card(UUID.randomUUID().toString(),"1234123412341234","1234","987","12","2025",true,false,1000,false,false,a1,Instant.now().toString());
		cr.save(c1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("cardNumber", "1234123412341234");
		requestParams.put("expiryMonth", "01");
		requestParams.put("expiryYear", "2025");
		requestParams.put("cardCryptogram", "123");
		requestParams.put("amount", 100);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/payments")
				.then()
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.extract().response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString("CARD_LOCKED_EXCEPTION"));
	}

	@Test
	public void invalidCardInfos(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		Card c1 = new Card(UUID.randomUUID().toString(),"1234123412341234","1234","987","12","2025",false,false,1000,false,false,a1,Instant.now().toString());
		cr.save(c1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("cardNumber", "1234123412341234");
		requestParams.put("expiryMonth", "01");
		requestParams.put("expiryYear", "2025");
		requestParams.put("cardCryptogram", "123");
		requestParams.put("amount", 100);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/payments")
				.then()
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.extract().response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString("INVALID_CARD_INFO"));
	}

	@Test
	public void notEnoughBalance(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		Card c1 = new Card(UUID.randomUUID().toString(),"1234123412341234","1234","987","12","2025",false,false,1000,false,false,a1,Instant.now().toString());
		cr.save(c1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("cardNumber", "1234123412341234");
		requestParams.put("expiryMonth", "12");
		requestParams.put("expiryYear", "2025");
		requestParams.put("cardCryptogram", "987");
		requestParams.put("amount", 100);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/payments")
				.then()
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.extract().response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString("NOT_ENOUGH_BALANCE"));
	}

	@Test
	public void paymentSuccess(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 100.0, i1);
		ar.save(a1);

		Card c1 = new Card(UUID.randomUUID().toString(),"1234123412341234","1234","987","12","2025",false,false,1000,false,false,a1,Instant.now().toString());
		cr.save(c1);


		JSONObject requestParams = new JSONObject();
		requestParams.put("cardNumber", "1234123412341234");
		requestParams.put("expiryMonth", "12");
		requestParams.put("expiryYear", "2025");
		requestParams.put("cardCryptogram", "987");
		requestParams.put("amount", 100);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/payments")
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();

	}

}
