package org.miage.norevolut.Norevolut;

import com.nimbusds.jose.shaded.json.JSONObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.miage.norevolut.NorevolutApplication;
import org.miage.norevolut.entities.Account;
import org.miage.norevolut.entities.Operation;
import org.miage.norevolut.entities.User;
import org.miage.norevolut.repositories.AccountRepository;
import org.miage.norevolut.repositories.CardRepository;
import org.miage.norevolut.repositories.OperationRepository;
import org.miage.norevolut.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;
import java.time.Instant;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OperationControllerTest {

	@LocalServerPort
	int port;

	@Autowired
	UserRepository ur;

	@Autowired
	OperationRepository or;

	@Autowired
	CardRepository cr;

	@Autowired
	AccountRepository ar;


	@BeforeEach
	public void setupContext(){
		or.deleteAll();
		cr.deleteAll();
		ar.deleteAll();
		ur.deleteAll();
		RestAssured.port = port;
		NorevolutApplication.testing = true;
	}

	@Test
	public void getAllOperationsOfAccount(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		Operation o1 = new Operation(UUID.randomUUID().toString(), Instant.now().toString(),"Carrefour",100.0,0.0,Iban.random(CountryCode.FR).toString(),a1,"Courses","FR",false,"");
		or.save(o1);

		Response response = when().get("/norevolut/accounts/" + a1.getIban()+ "/operations")
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract()
				.response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString(iban));
		assertThat(jsonAsString, containsString("Carrefour"));
	}

	@Test
	public void getOneOperation(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		Operation o1 = new Operation(UUID.randomUUID().toString(), Instant.now().toString(),"Aliexpress",50.0,0.25,Iban.random(CountryCode.CZ).toString(),a1,"Courses","CZ",false,"");
		or.save(o1);

		Response response = when().get("/norevolut/accounts/" + a1.getIban()+ "/operations/" + o1.getId())
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract()
				.response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString(iban));
		assertThat(jsonAsString, containsString("Aliexpress"));
		assertThat(jsonAsString, containsString("CZ"));
	}

	@Test
	public void notEnoughBalanceTest(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("label", "Paiement mais pas de sous");
		requestParams.put("amount", 20000.0);
		requestParams.put("creditedAccount", Iban.random(CountryCode.FR).toString());
		requestParams.put("category", "Investissement");
		requestParams.put("country", "FR");
		requestParams.put("transfert", false);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/accounts/" + a1.getIban() + "/operations")
				.then()
				.statusCode(HttpStatus.SC_BAD_REQUEST)
				.extract().response();

		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString("NOT_ENOUGH_BALANCE"));
	}

	@Test
	public void notAllowedUser(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 1000.0, i1);
		ar.save(a1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("label", "Utilisateur non autorisé");
		requestParams.put("amount", 100);
		requestParams.put("creditedAccount", Iban.random(CountryCode.FR).toString());
		requestParams.put("category", "Investissement");
		requestParams.put("country", "FR");
		requestParams.put("transfert", false);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/accounts/" + a1.getIban() + "/operations")
				.then()
				.statusCode(HttpStatus.SC_FORBIDDEN)
				.extract().response();

		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString("USER_NOT_ALLOWED_FOR_OPERATION"));
	}

	@Test
	public void sucessOperation(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		NorevolutApplication.testingUser = i1.getId();

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 1000.0, i1);
		ar.save(a1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("label", "Utilisateur non autorisé");
		requestParams.put("amount", 100);
		requestParams.put("creditedAccount", Iban.random(CountryCode.FR).toString());
		requestParams.put("category", "Investissement");
		requestParams.put("country", "FR");
		requestParams.put("transfert", false);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/accounts/" + a1.getIban() + "/operations")
				.then()
				.statusCode(HttpStatus.SC_CREATED)
				.extract().response();
	}


}
