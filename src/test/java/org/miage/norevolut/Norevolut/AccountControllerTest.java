package org.miage.norevolut.Norevolut;

import com.nimbusds.jose.shaded.json.JSONObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.miage.norevolut.NorevolutApplication;
import org.miage.norevolut.entities.Account;
import org.miage.norevolut.entities.User;
import org.miage.norevolut.repositories.AccountRepository;
import org.miage.norevolut.repositories.CardRepository;
import org.miage.norevolut.repositories.OperationRepository;
import org.miage.norevolut.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.UUID;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountControllerTest {

	@LocalServerPort
	int port;

	@Autowired
	UserRepository ur;

	@Autowired
	OperationRepository or;

	@Autowired
	CardRepository cr;

	@Autowired
	AccountRepository ar;


	@BeforeEach
	public void setupContext(){
		or.deleteAll();
		cr.deleteAll();
		ar.deleteAll();
		ur.deleteAll();
		RestAssured.port = port;
		NorevolutApplication.testing = true;
	}

	@Test
	public void getOneAccount(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		Response response = when().get("/norevolut/accounts/" + a1.getIban())
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract()
				.response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString(iban));
	}

	@Test
	public void getAllAccounts(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban1 = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban1, "FR", "EUR", 0.0, i1);
		ar.save(a1);
		String iban2 = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a2 = new Account(iban2, "FR", "EUR", 0.0, i1);
		ar.save(a2);
		String iban3 = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a3 = new Account(iban3, "FR", "EUR", 0.0, i1);
		ar.save(a3);
		Response response = when().get("/norevolut/accounts")
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract()
				.response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString(iban1));
		assertThat(jsonAsString, containsString(iban2));
		assertThat(jsonAsString, containsString(iban3));

	}

	@Test
	public void saveAccount(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		NorevolutApplication.testingUser = i1.getId();
		JSONObject requestParams = new JSONObject();
		requestParams.put("country", "CH");

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/accounts")
				.then()
				.statusCode(HttpStatus.SC_CREATED)
				.extract().response();

		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString(i1.getId()));


	}

	@Test
	public void patchAccount(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban1 = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban1, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		NorevolutApplication.testingUser = i1.getId();
		JSONObject requestParams = new JSONObject();
		requestParams.put("country", "DE");

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.patch("/norevolut/accounts/"  + iban1)
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();

		String jsonAsString = response.asString();
		Assertions.assertEquals(200, response.statusCode());
		assertThat(jsonAsString, containsString(i1.getId()));
		assertThat(jsonAsString, containsString("DE"));

	}


}
