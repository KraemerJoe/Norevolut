package org.miage.norevolut.Norevolut;

import com.nimbusds.jose.shaded.json.JSONObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.miage.norevolut.NorevolutApplication;
import org.miage.norevolut.entities.Account;
import org.miage.norevolut.entities.Card;
import org.miage.norevolut.entities.Operation;
import org.miage.norevolut.entities.User;
import org.miage.norevolut.repositories.AccountRepository;
import org.miage.norevolut.repositories.CardRepository;
import org.miage.norevolut.repositories.OperationRepository;
import org.miage.norevolut.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;
import java.time.Instant;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CardControllerTest {

	@LocalServerPort
	int port;

	@Autowired
	UserRepository ur;

	@Autowired
	OperationRepository or;

	@Autowired
	CardRepository cr;

	@Autowired
	AccountRepository ar;


	@BeforeEach
	public void setupContext(){
		or.deleteAll();
		cr.deleteAll();
		ar.deleteAll();
		ur.deleteAll();
		RestAssured.port = port;
		NorevolutApplication.testing = true;
	}

	@Test
	public void getAllCardsOfAccount(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		Operation o1 = new Operation(UUID.randomUUID().toString(), Instant.now().toString(),"Carrefour",100.0,0.0,Iban.random(CountryCode.FR).toString(),a1,"Courses","FR",false,"");
		or.save(o1);

		Card c1 = new Card(UUID.randomUUID().toString(),"1234123412341234","1234","987","12","2025",false,false,1000,false,false,a1,Instant.now().toString());
		cr.save(c1);
		Card c2 = new Card(UUID.randomUUID().toString(),"7896789678967896","1234","987","12","2025",false,false,1000,false,false,a1,Instant.now().toString());
		cr.save(c2);

		Response response = when().get("/norevolut/accounts/" + a1.getIban()+ "/cards")
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract()
				.response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString(iban));
		assertThat(jsonAsString, containsString("1234123412341234"));
		assertThat(jsonAsString, containsString("7896789678967896"));
	}

	@Test
	public void getOneCard(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		Operation o1 = new Operation(UUID.randomUUID().toString(), Instant.now().toString(),"Carrefour",100.0,0.0,Iban.random(CountryCode.FR).toString(),a1,"Courses","FR",false,"");
		or.save(o1);

		Card c1 = new Card(UUID.randomUUID().toString(),"1234123412341234","1234","987","12","2025",false,false,1000,false,false,a1,Instant.now().toString());
		cr.save(c1);


		Response response = when().get("/norevolut/accounts/" + a1.getIban()+ "/cards/"+ c1.getId())
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract()
				.response();
		String jsonAsString = response.asString();
		assertThat(jsonAsString, containsString(iban));
		assertThat(jsonAsString, containsString("1234123412341234"));

	}

	@Test
	public void saveCardForbiddenUser(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("virtual", false);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/accounts/" + a1.getIban() + "/cards")
				.then()
				.statusCode(HttpStatus.SC_FORBIDDEN)
				.extract().response();
	}

	@Test
	public void saveCardSuccess(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		NorevolutApplication.testingUser = i1.getId();

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 0.0, i1);
		ar.save(a1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("virtual", false);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.post("/norevolut/accounts/" + a1.getIban() + "/cards")
				.then()
				.statusCode(HttpStatus.SC_CREATED)
				.extract().response();
	}

	@Test
	public void patchCard(){
		User i1 = new User(UUID.randomUUID().toString(), "Sawyer", "Tom", "test@gmail.com", "54000", "003364869966", "");
		ur.save(i1);

		String iban = Iban.random(CountryCode.getByCode("FR")).toString();
		Account a1 = new Account(iban, "FR", "EUR", 1000.0, i1);
		ar.save(a1);

		Card c1 = new Card(UUID.randomUUID().toString(),"1234123412341234","1234","987","12","2025",false,false,1000,false,false,a1,Instant.now().toString());
		cr.save(c1);

		JSONObject requestParams = new JSONObject();
		requestParams.put("locked", true);

		Response response = given()
				.header("Content-type", "application/json")
				.and()
				.body(requestParams.toJSONString())
				.when()
				.patch("/norevolut/accounts/" + a1.getIban() + "/cards/" + c1.getId())
				.then()
				.statusCode(HttpStatus.SC_OK)
				.extract().response();

	}


}
